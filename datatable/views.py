from flask import Flask, request, render_template, jsonify, json, Blueprint
from flaskext.mysql import MySQL #pip install flask-mysql
import pymysql

app = Flask(__name__)
hh=Blueprint('datatable', __name__,template_folder='templates',static_folder='static', static_url_path='datatable/static',url_prefix='' )
    
mysql = MySQL()
   
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'btph'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)
  
@hh.route('ee/')
def ff():
    return render_template('datatable.html')
@hh.route('/ee/French.json')
def hj():
    return render_template('French.json')
 
@hh.route("/ee/ajaxfile",methods=["POST","GET"])
def ajaxfile():
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        if request.method == 'POST':
            draw = request.form['draw'] 
            row = int(request.form['start'])
            rowperpage = int(request.form['length'])
            searchValue = request.form["search[value]"]
            print(draw)
            print(row)
            print(rowperpage)
            print(searchValue)
 
            ## Total number of records without filtering
            cursor.execute("select count(*) as allcount from mycard")
            rsallcount = cursor.fetchone()
            totalRecords = rsallcount['allcount']
            print(totalRecords) 
 
            ## Total number of records with filtering
            likeString = "%" + searchValue +"%"
            cursor.execute("SELECT count(*) as allcount from mycard WHERE name LIKE %s OR position LIKE %s OR office LIKE %s", (likeString, likeString, likeString))
            rsallcount = cursor.fetchone()
            totalRecordwithFilter = rsallcount['allcount']
            print(totalRecordwithFilter) 
 
            ## Fetch records
            if searchValue=='':
                cursor.execute("SELECT * FROM mycard ORDER BY name asc limit %s, %s;", (row, rowperpage))
                employeelist = cursor.fetchall()
            else:        
                cursor.execute("SELECT * FROM mycard WHERE name LIKE %s OR position LIKE %s OR office LIKE %s limit %s, %s;", (likeString, likeString, likeString, row, rowperpage))
                employeelist = cursor.fetchall()
 
            data = []
 
            for row in employeelist:
                data.append({
                    'id':row['id'],
                    'name': row['name'],
                    'image': row['position'],
                    'fonction': row['age'],
                    'groupe_s': row['salary'],
                    'matricule': row['office'],
                    'delete': f'<a href="javascript:void();"   data-id="{row["id"]}" class="btn btn-danger btn-sm deleteBtn" >supprimer</a>',

                })
 
            response = {
                'draw': draw,
                'iTotalRecords': totalRecords,
                'iTotalDisplayRecords': totalRecordwithFilter,
                'aaData': data,
            }
            return jsonify(response)
    except Exception as e:
        print(e)
    finally:
        cursor.close() 
        conn.close()
@dash.route("/ee/edit/<int:id>", methods=["PUT"])
def edit_employee(id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        if request.method == 'PUT':
            # Example: Update the employee's salary
            new_salary = request.form.get('new_salary')
            cursor.execute("UPDATE employee SET salary=%s WHERE id=%s;", (new_salary, id))
            conn.commit()
            return jsonify({'message': 'Employee updated successfully'})
    except Exception as e:
        print(e)
        return jsonify({'error': 'An error occurred'}), 500
    finally:
        cursor.close() 
        conn.close()

@dash.route("/ee/delete/<int:id>", methods=["DELETE"])
def delete_employee(id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        if request.method == 'DELETE':
            cursor.execute("DELETE FROM mycard WHERE id=%s;", (id,))
            conn.commit()
            data = {'status': 'true'}
            return jsonify(data)
    except Exception as e:
        print(e)
        return jsonify({'error': 'An error occurred'}), 500
    finally:
        cursor.close() 
        conn.close()

if __name__ == "__main__":
    app.run()